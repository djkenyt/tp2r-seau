I)
1) Protocole ICMP
2) 
3 Il n'attribue aucune ip contrairement au routeur.

4) ARP: ARP Request-> ARP Reply -> Ajout de L'@MAC dans la table.

Ping : Machine a l'origine du ping -> routeur -> DNS (facultatif) -> 

IP de destination

5) Un switch n'as pas beosin d'ip car il sert juste à interconnecter des machines
entre elles.

Les machines ont besoin d'une @IP pour s'identifier sur le réseau et voir qui est qui.

II)
1) 
   1    0050.7966.6800    DYNAMIC       Et0/0
   
   1    0050.7966.6801    DYNAMIC       Et0/1
   
   1    0050.7966.6802    DYNAMIC       Et0/2
   
VLAN    @MAC              DHCP/STATIQUE Port utilisé

2)
VLAN0001

  Spanning tree enabled protocol rstp
  
  Root ID    Priority    4097
  
             Address     aabb.cc00.0100
             
             This bridge is the root
             
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             

  Bridge ID  Priority    4097   (priority 4096 sys-id-ext 1)
  
             Address     aabb.cc00.0100
             
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             
             Aging Time  300 sec
             

Interface           Role Sts Cost      Prio.Nbr Type

------------------- ---- --- --------- -------- --------------------------------

Et0/0               Desg FWD 100       128.1    Shr

Et0/1               Desg FWD 100       128.2    Shr

Et0/2               Desg FWD 100       128.3    Shr

Et0/3               Desg FWD 100       128.4    Shr

Et1/0               Desg FWD 100       128.5    Shr

Et1/1               Desg FWD 100       128.6    Shr

Et1/2               Desg FWD 100       128.7    Shr

Et1/3               Desg FWD 100       128.8    Shr

Et2/0               Desg FWD 100       128.9    Shr

Et2/1               Desg FWD 100       128.10   Shr

Et2/2               Desg FWD 100       128.11   Shr

Et2/3               Desg FWD 100       128.12   Shr

Et3/0               Desg FWD 100       128.13   Shr

Et3/1               Desg FWD 100       128.14   Shr

Et3/2               Desg FWD 100       128.15   Shr

Et3/3               Desg FWD 100       128.16   Shr

IOU1#show spanning-tree bridge

                                                   Hello  Max  Fwd
                                                   
Vlan                         Bridge ID              Time  Age  Dly  Protocol

---------------- --------------------------------- -----  ---  ---  --------

VLAN0001          4097 ( 4096,   1) aabb.cc00.0100    2    20   15  rstp

IOU1#show spanning-tree summary

Switch is in rapid-pvst mode

Root bridge for: VLAN0001

Extended system ID                      is enabled

Portfast Default                        is disabled

Portfast Edge BPDU Guard Default        is disabled

Portfast Edge BPDU Filter Default       is disabled

Loopguard Default                       is disabled

PVST Simulation Default                 is enabled but inactive in rapid-pvst mo
de

Bridge Assurance                        is enabled

EtherChannel misconfig guard            is enabled

Configured Pathcost method used is short
UplinkFast                              is disabled

BackboneFast                            is disabled

Name                   Blocking Listening Learning Forwarding STP Active

---------------------- -------- --------- -------- ---------- ----------

VLAN0001                     0         0        0         16         16

---------------------- -------- --------- -------- ---------- ----------

1 vlan                       0         0        0         16         16



III)
1)PC2) 84 bytes from 10.2.3.3 icmp_seq=1 ttl=64 time=0.221 ms

PC1) host (10.2.3.2) not reachable
host (10.2.3.3) not reachable

2) 
PC1) 84 bytes from 10.2.10.3 icmp_seq=1 ttl=64 time=0.276 ms
host (10.2.20.4) not reachable

PC2) 84 bytes from 10.2.20.4 icmp_seq=1 ttl=64 time=0.186 ms
host (10.2.10.3) not reachable

IV)
IOU1#configure t

IOU1(config)#interface range fastEthernet 0/1 - 2

IOU1(config-if-range)#channel-group 1 mode active

IOU1(config-if-range)#exit

IOU1(config)#interface port-channel 1

IOU1(config-if)#switchport mode trunk

IOU1(config-if)#switchport trunk allowed vlan 10,20